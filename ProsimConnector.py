import sdc11073
import serial
from sdc11073.pmtypes import CodedValue
from sdc11073.roles.product import BaseProduct
from sdc11073.roles.providerbase import ProviderRole


class ProsimConnectorProviderRole(ProviderRole):
    """ This provider handles operations with code == MY_CODE_1 and MY_CODE_2.
    Operations with these codes already exist in the mdib that is used for this test. """

    def __init__(self, log_prefix):
        super().__init__(log_prefix)
        self.ser = serial.Serial('COM5', 115200, timeout=100, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
                                 bytesize=8, rtscts=1)
        self.ser.write(b'REMOTE\r\n')

    def makeOperationInstance(self, operationDescriptorContainer):
        if type(operationDescriptorContainer) == sdc11073.mdib.descriptorcontainers.ActivateOperationDescriptorContainer:
            operation = self._mkOperationFromOperationDescriptor(operationDescriptorContainer,
                                                                 currentArgumentHandler=self._activateOperation)
            return operation
        elif type(
                operationDescriptorContainer) == sdc11073.mdib.descriptorcontainers.SetStringOperationDescriptorContainer:
            operation = self._mkOperationFromOperationDescriptor(operationDescriptorContainer,
                                                                 currentArgumentHandler=self._setMetric)
            return operation

        elif type(
                operationDescriptorContainer) == sdc11073.mdib.descriptorcontainers.SetValueOperationDescriptorContainer:
            operation = self._mkOperationFromOperationDescriptor(operationDescriptorContainer,
                                                                 currentArgumentHandler=self._setMetric)
            return operation
        else:
            return None

    def _activateOperation(self, operationInstance, argument):
        self._mdib.mdibUpdateTransaction()
        operationMdcCode = operationInstance._codedValue.code
        cmd = handle2cmdDict[operationMdcCode][0]

        for x in range(1, len(handle2cmdDict[operationMdcCode])):
            mdcCode = handle2cmdDict[operationMdcCode][x]
            value = None

            for state in operationInstance.operationTargetStorage.objects:
                if type(state) == sdc11073.mdib.statecontainers.EnumStringMetricStateContainer or type(
                        state) == sdc11073.mdib.statecontainers.NumericMetricStateContainer or type(
                        state) == sdc11073.mdib.statecontainers.StringMetricStateContainer:
                    if mdcCode == state.descriptorContainer.Type.Code:
                        value = state.metricValue.Value

            if value == None:
                print("Error Metric is null")

            if (x == 1):
                cmd = cmd + "=" + value
            else:
                cmd = cmd + "," + value

        cmd = cmd + '\r\n'

    def _setMetric(self, operationInstance, argument):
        with self._mdib.mdibUpdateTransaction() as mgr:
            my_state = mgr.getMetricState(operationInstance.operationTarget)
            if my_state.metricValue is None:
                my_state.mkMetricValue()
            my_state.metricValue.Value = argument


class ProsimConnector(BaseProduct):
    def __init__(self, log_prefix=None):
        super().__init__(log_prefix)
        self.prosimconnectorproviderrole = ProsimConnectorProviderRole(log_prefix=log_prefix)
        self._ordered_providers.append(self.prosimconnectorproviderrole)


handle2cmdDict = {
    "mdc_x_act_aclswave": ["ACLSWAVE", "mdc_x_aclswave"],
    "mdc_x_act_afib": ["AFIB"],
    "mdc_x_act_afib2": ["AFIB2"],
    "mdc_x_act_ambf": ["AMBF"],
    "mdc_x_act_ambm": ["AMBM"],
    "mdc_x_act_ambs": ["AMBS"],
    "mdc_x_act_cndwave": ["CNDWAVE"],
    "mdc_x_act_cobase": ["COBASE"],
    "mdc_x_act_coinj": ["COINJ"],
    "mdc_x_act_corun": ["CORUN"],
    "mdc_x_act_cowave": ["COWAVE"],
    "mdc_x_act_eart": ["EART"],
    "mdc_x_act_eartld": ["EARTLD"],
    "mdc_x_act_eartsz": ["EARTSZ"],
    "mdc_x_act_ecgampl": ["ECGAMPL"],
    "mdc_x_act_ecgrun": ["ECGRUN"],
    "mdc_x_act_ehafibf": ["EHAFIBF"],
    "mdc_x_act_ehafibs": ["EHAFIBS"],
    "mdc_x_act_ehafl100": ["EHAFL100"],
    "mdc_x_act_ehafl150": ["EHAFL150"],
    "mdc_x_act_ehafl43": ["EHAFL43"],
    "mdc_x_act_ehafl50": ["EHAFL50"],
    "mdc_x_act_ehafl60": ["EHAFL60"],
    "mdc_x_act_ehafl75": ["EHAFL75"],
    "mdc_x_act_ibpartm": ["IBPARTM"],
    "mdc_x_act_ibpartp": ["IBPARTP"],
    "mdc_x_act_ibpp": ["IBPP"],
    "mdc_x_act_ibps": ["IBPS"],
    "mdc_x_act_ibpsns": ["IBPSNS"],
    "mdc_x_act_ibpw": ["IBPW"],
    "mdc_x_act_ident": ["IDENT"],
    "mdc_x_act_lkoff": ["LKOFF"],
    "mdc_x_act_lkstat": ["LKSTAT"],
    "mdc_x_act_local": ["LOCAL"],
    "mdc_x_act_monovtach": ["MONOVTACH"],
    "mdc_x_act_nibpes": ["NIBPES"],
    "mdc_x_act_nibpleak": ["NIBPLEAK"],
    "mdc_x_act_nibpp": ["NIBPP"],
    "mdc_x_act_nibppop": ["NIBPPOP"],
    "mdc_x_act_nibprun": ["NIBPRUN"],
    "mdc_x_act_nibpv": ["NIBPV"],
    "mdc_x_act_nsra": ["NSRA", "mdc_x_nsra"],
    "mdc_x_act_nsrax": ["NSRAX", "mdc_x_nsrax"],
    "mdc_x_act_nsrp": ["NSRP"],
    "mdc_x_act_perf": ["PERF"],
    "mdc_x_act_polyvtach": ["POLYVTACH"],
    "mdc_x_act_popoff": ["POPOFF"],
    "mdc_x_act_prewave": ["PREWAVE"],
    "mdc_x_act_ps": ["PS"],
    "mdc_x_act_pst": ["PST"],
    "mdc_x_act_pulse": ["PULSE"],
    "mdc_x_act_qbat": ["QBAT"],
    "mdc_x_act_qmode": ["QMODE"],
    "mdc_x_act_qrs": ["QRS"],
    "mdc_x_act_qurcurve": ["QURCURVE"],
    "mdc_x_act_rdet": ["RDET"],
    "mdc_x_act_remote": ["REMOTE"],
    "mdc_x_act_reset": ["RESET"],
    "mdc_x_act_respampl": ["RESPAMPL"],
    "mdc_x_act_respapnea": ["RESPAPNEA"],
    "mdc_x_act_respbase": ["RESPBASE"],
    "mdc_x_act_resplead": ["RESPLEAD"],
    "mdc_x_act_respm": ["RESPM"],
    "mdc_x_act_resprate": ["RESPRATE"],
    "mdc_x_act_respratio": ["RESPRATIO"],
    "mdc_x_act_resprun": ["RESPRUN"],
    "mdc_x_act_resps": ["RESPS"],
    "mdc_x_act_respwave": ["RESPWAVE"],
    "mdc_x_act_sat": ["SAT"],
    "mdc_x_act_serialnumber": ["SERIALNUMBER"],
    "mdc_x_act_sine": ["SINE"],
    "mdc_x_act_spo2type": ["SPO2TYPE"],
    "mdc_x_act_spo2utype": ["SPO2UTYPE"],
    "mdc_x_act_spvwave": ["SPVWAVE"],
    "mdc_x_act_square": ["SQUARE"],
    "mdc_x_act_stdev": ["STDEV", "mdc_x_stdev"],
    "mdc_x_act_tallt": ["TALLT"],
    "mdc_x_act_temp": ["TEMP"],
    "mdc_x_act_trans": ["TRANS"],
    "mdc_x_act_tri": ["TRI"],
    "mdc_x_act_tvpampl": ["TVPAMPL", "mdc_x_tvpampl_chamber", "mdc_x_tvpampl_amplitude"],
    "mdc_x_act_tvppol": ["TVPPOL", "mdc_x_tvppol_chamber", "mdc_x_tvppol_polarity"],
    "mdc_x_act_tvpwave": ["TVPWAVE"],
    "mdc_x_act_tvpwid": ["TVPWID", "mdc_x_tvpwid_chamber", "mdc_x_tvpwid_width"],
    "mdc_x_act_vfib": ["VFIB"],
    "mdc_x_act_vfib1": ["VFIB1"],
    "mdc_x_act_vfib2": ["VFIB2"],
    "mdc_x_act_vntwave": ["VNTWAVE"]
}
