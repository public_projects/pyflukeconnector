import uuid
import time

import sdc11073.mdib.descriptorcontainers
from sdc11073 import pmtypes
from sdc11073.namespaces import domTag
from sdc11073.pmtypes import AllowedValue
from sdc11073.sdcdevice import SdcDevice
from sdc11073.mdib import DeviceMdibContainer
from sdc11073.pysoap.soapenvelope import DPWSThisModel, DPWSThisDevice
from sdc11073.location import SdcLocation
from sdc11073.wsdiscovery import WSDiscoverySingleAdapter
from ProsimConnector import ProsimConnector

baseUUID = uuid.UUID('{cc013678-79f6-666c-998f-3cc0cc050230}')
my_uuid = uuid.uuid5(baseUUID, "67890")

def setLocalEnsembleContext(mdib, ensemble):
    descriptorContainer = mdib.descriptions.NODETYPE.getOne(domTag('EnsembleContextDescriptor'))
    if not descriptorContainer:
        print("No ensemble contexts in mdib")
        return
    allEnsembleContexts = mdib.contextStates.descriptorHandle.get(descriptorContainer.handle, [])
    with mdib.mdibUpdateTransaction() as mgr:
        # set all to currently associated Locations to Disassociated
        associatedEnsembles = [l for l in allEnsembleContexts if
                               l.ContextAssociation == pmtypes.ContextAssociation.ASSOCIATED]
        for l in associatedEnsembles:
            ensembleContext = mgr.getContextState(l.descriptorHandle, l.Handle)
            ensembleContext.ContextAssociation = pmtypes.ContextAssociation.DISASSOCIATED
            ensembleContext.UnbindingMdibVersion = mdib.mdibVersion  # UnbindingMdibVersion is the first version in which it is no longer bound ( == this version)

        newEnsState = mgr.getContextState(descriptorContainer.handle)  # this creates a new location state
        newEnsState.ContextAssociation = 'Assoc'
        newEnsState.Identification = [pmtypes.InstanceIdentifier(root="1.2.3", extensionString=ensemble)]

if __name__ == '__main__':
    myDiscovery = WSDiscoverySingleAdapter("WLAN")
    myDiscovery.start()
    my_mdib = DeviceMdibContainer.fromMdibFile("mdib_prosim.xml")
    print ("My UUID is {}".format(my_uuid))
    my_location = SdcLocation(fac='ICCAS', poc='2OG', bed='1336')

    dpwsModel = DPWSThisModel(manufacturer='ICCAS',
                              manufacturerUrl='www.iccas.de',
                              modelName='FlukeProsim8Connector',
                              modelNumber='1.0',
                              modelUrl='www.iccas.de/model',
                              presentationUrl='www.iccas.de/model/presentation')
    dpwsDevice = DPWSThisDevice(friendlyName='FlukeProsim8Connector',
                                firmwareVersion='Version1',
                                serialNumber='666')

    my_product_impl = ProsimConnector(log_prefix='pf8')

    sdcDevice = SdcDevice(ws_discovery=myDiscovery,
                            my_uuid=my_uuid,
                            model=dpwsModel,
                            device=dpwsDevice,
                            deviceMdibContainer=my_mdib,
                            roleProvider = my_product_impl)

    for description in my_mdib.descriptions.objects:
        if type(description) == sdc11073.mdib.descriptorcontainers.EnumStringMetricDescriptorContainer:
            for state in my_mdib.states.objects:
                if state.descriptorHandle == description.Handle:
                    values = []
                    for allowedValue in description.AllowedValue:
                        values.append(allowedValue.value)
                    if values:
                        state.mkMetricValue()
                        state.metricValue.Value = values[0]
                        state.metricValue.ActiveDeterminationPeriod = "1494554822450"
                        state.metricValue.Validity = 'Vld'
                        state.ActivationState = "On"
                        state.AllowedValues = values

    sdcDevice.startAll()
    setLocalEnsembleContext(my_mdib, "MyEnsemble")
    sdcDevice.setLocation(my_location)

    while True:
        time.sleep(5)